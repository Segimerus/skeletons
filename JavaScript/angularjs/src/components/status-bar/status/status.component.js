// import statusTemplate from "./components/status-bar/status/status.template.html"
import statusTemplate from "./status.template.html"

export default
 {
     template: statusTemplate,
     bindings: {
         featureObject:'<'
     }
     //@ one way binding, only string
     //= two way binding, parent scope is also updated
     //< one way binding, expression
     //& one way binding, binds the expression of the parent scope to the child's function as parameter definition. The actual values for the parameters are defined in the child's scope
 };