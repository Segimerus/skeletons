import angular from 'angular'

import "CommonStyles/main-page.scss"

import statusBarCDO from 'Components/status-bar/status-bar.component.js'
import statusCDO from 'Components/status-bar/status/status.component.js'

var mainModule = angular.module("mainApp", []);

mainModule.component("statusComponent", statusCDO);
mainModule.component("statusBarComponent", statusBarCDO);

angular.bootstrap(document, ["mainApp"]);