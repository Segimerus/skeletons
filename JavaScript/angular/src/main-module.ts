import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {StatusBar}  from 'components/status-bar/status-bar.component';
import {Status} from 'components/status-bar/status/status.component'
import 'common/styles/main-page.scss'

@NgModule({
    imports:      [ BrowserModule ],
    declarations: [ StatusBar, Status ],
    bootstrap:    [ StatusBar ]
})
export default class MainModule { }
