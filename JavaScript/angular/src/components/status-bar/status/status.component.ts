import statusTemplate from "./status.template.html"
import { Component, Input } from '@angular/core';

@Component({
    selector: 'status',
    template: statusTemplate
})

export class Status
{
    @Input() featureObject:Array<{name:string,status:string}>;

}