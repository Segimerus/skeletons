import React from "react";
import './store-tester.scss'
import {Unsubscribe} from "redux";
import {MainStore} from "../../state-management/main-store";

interface StoreTesterState
{
	incrementor: number
}

export class StoreTesterComponent extends React.Component<{},StoreTesterState>
{
    protected readonly _unsubscribe: Unsubscribe;

    constructor(props: {})
    {
        super(props);

        this.state = {incrementor: 1};

        this._unsubscribe = MainStore.STORE.counter.watch((newState: number) =>
        {
           this.forceUpdate();
           console.log(`State changed:`, newState, MainStore.STORE.mainState.getState());
        });

    }

    render()
    {
        const currentVal = MainStore.STORE.counter.getState();

        return (
            <div className="storeTester">
                <div className="storeCaption">Store Tester:</div>
                <input type="text" onChange={this.incChange} value={this.state.incrementor}/>
                <button onClick={this.inc}>+</button>
                <button onClick={this.dec}>-</button>
                <div className="storeVal">{currentVal}</div>
            </div>
        )
    }

    componentWillUnmount()
    {
        this._unsubscribe();
    }

    inc = () =>
    {
        MainStore.STORE.counter.dispatch(this.state.incrementor);
    };

    dec = () =>
    {
        MainStore.STORE.counter.dispatch(-1 * this.state.incrementor);
    };

    incChange =(event : React.FormEvent<HTMLInputElement>) =>
    {
        this.setState({incrementor: parseInt(event.currentTarget.value)});
    };
}
