import React from 'react'
import grumpy from "../../../resources/images/grumpy.jpg"

export class SimpleComponent extends React.Component
{
	render()
	{
		return(
			<div>
				<h1>Simple Component</h1>
				<img src={grumpy} alt="Grumpy"/>
			</div>
		)
	}
}
