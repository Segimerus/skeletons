import React from 'react'
import "./list-element.scss"

export interface ListElementProps
{
	name: string,
	component: JSX.Element
	id: number
	onClick: (id: number) => void
}

export class ListElement extends React.Component<ListElementProps>
{
	constructor(props: ListElementProps)
	{
		super(props);
	}

	render()
	{
		return (
			<div>
				<a href="#"  className="list-element" onClick={() => this.props.onClick(this.props.id)}>{this.props.name}</a>
			</div>
		)

	}
}
