import React from 'react'
import { ListElement } from "./list-element/list-element";
import './main-container.scss'
import {ListElementData} from "./list-data";

export interface MainContainerProps
{
	data: ListElementData[]
}

export interface MainContainerState
{
	shownComponent: number
}

export class MainContainer extends React.Component<MainContainerProps, MainContainerState>
{
	constructor(props: MainContainerProps)
	{
		super(props);

		this.state ={shownComponent: 0};
	}

	render()
	{
		const currentComponent = this.props.data[this.state.shownComponent].component;

		return (
			<div id="mainWrapper">
				<div id="componentList">{this.getList()}</div>
				<div id="presenter">{currentComponent}</div>
			</div>

		)
	}

	onListElementClick = (id: number) =>
	{
		this.setState({shownComponent: id})
	}

	getList():JSX.Element[]
	{
		return (this.props.data.map((listElementProps: ListElementData, index: number) =>
		{
			return (<ListElement key={"listElement-" + index} name={listElementProps.name} component={listElementProps.component} id={index} onClick={this.onListElementClick}/>)
		}))
	}
}
