import React from 'react'
import {SimpleComponent} from "../components/simple-component/simple-component";
import {StoreTesterComponent} from "../components/store-tester/store-tester";
import {PersonForm} from "../components/form/person-form";

export interface ListElementData
{
	name: string,
	component: JSX.Element
}
export const LIST_DATA: ListElementData[] = [
	{name: "Simple Component", component: <SimpleComponent />},
	{name: "Store Tester Component", component: <StoreTesterComponent />},
	{name: "Form", component: <PersonForm />}
];
