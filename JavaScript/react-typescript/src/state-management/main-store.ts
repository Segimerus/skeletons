import {ActionWrapper, StateStore, StateWrapper} from "./state-store";

export interface MainStoreState
{
    counter: { val: number }
}

export class MainStore
{
    public static readonly STORE = new MainStore();

    private readonly _store: StateStore<MainStoreState>;

    public readonly counter: ActionWrapper<number>;
    public readonly mainState: StateWrapper<MainStoreState>;

    private constructor()
    {
        this._store = new StateStore<MainStoreState>({counter: {val: 0}});
        this.counter = this.createCounterWrapper();
        this.mainState = this.createMainStateWrapper();
    }

    private createMainStateWrapper(): StateWrapper<MainStoreState>
    {
        return {
            getState: () => this._store.getState(""),
            watch: (onChange: (newState: MainStoreState) => void) => this._store.watch("", onChange)
        }
    }

    private createCounterWrapper(): ActionWrapper<number>
    {
        const path = "counter.val";
        const template = (payload: number) => ({
            type: "COUNTER",
            payload: payload,
            reducer: (state: MainStoreState): MainStoreState => {
                const newVal = state.counter.val + payload;
                return {...state, counter: {val: newVal}};
            }
        });

        return {
            getState: () => this._store.getState(path),
            watch: (onChange : (newState: number) => void) => this._store.watch(path, onChange),
            dispatch: (payload: number) => this._store.dispatch(template, payload)
        }
    }


}