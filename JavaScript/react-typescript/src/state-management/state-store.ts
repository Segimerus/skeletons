import {AnyAction, createStore, Store, Unsubscribe} from 'redux';
import {get} from 'lodash'

export interface ActionData<S>
{
    reducer: (state: S) => S
    payload: any
    type: string
}

export type ActionCreator<S> = (payload: any) => ActionData<S>

export interface ActionWrapper<T> extends StateWrapper<T>
{
    dispatch: (payload: T) => void
}

export interface StateWrapper<T>
{
    getState: () => T
    watch: (onChange: (newState: T) => void) => Unsubscribe
}

export class StateStore<S>
{
    private readonly _store: Store<S>;

    constructor(initialState: S)
    {
        this._store = createStore(this.reducer, initialState)
    }

	/*defines how state changes from current to next*/
	private reducer(state: S, action: AnyAction): S
    {
        if(action.type === "@@redux/INIT")
        {
            return Object.assign({}, state)
        }
        else
        {
            return (action as ActionData<S>).reducer(state);
        }
    }

	/*watch state for changes*/
	public watch(path: string, onChange: (newState: any) => void): Unsubscribe
    {
        let currentState = this.getState(path);

        const handleChange = () =>
        {
            let nextState = this.getState(path);

            if (nextState !== currentState)
            {
                currentState = nextState;
                onChange(currentState);
            }
        };

        return this._store.subscribe(handleChange);
    }

	/*initiate a statechange with an action*/
	public dispatch(action:ActionCreator<S>, payload: any)
    {
        this._store.dispatch(action(payload));
    }

    //make this an enum
	/*get current state on the path*/
	public getState(path: string = ""): any
    {
        const currentState = this._store.getState();

        return get(currentState, path, currentState);
    }
}

