import ReactDOM from 'react-dom'
import React from 'react'
import { MainContainer } from "./main-container/main-container";
import { LIST_DATA } from "./main-container/list-data";
import "./index.scss"

renderUI();

function renderUI()
{
    ReactDOM.render(
        <div>
            <MainContainer data={LIST_DATA}></MainContainer>
        </div>, document.querySelector("#appContainer")
    );
}
