const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const isProduction = process.argv.indexOf('-p') !== -1;
const hash = isProduction ? ".[hash]" : "";

//Directories
const srcDir = path.join(__dirname, 'src');
const distDir = path.join(__dirname, 'dist');
const resDir  = path.join(__dirname, 'resources');
const imagesDir  = path.join(resDir, 'images');


const config = {
    mode: isProduction ? 'production' : 'development',
    optimization: {
        namedModules: true
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js", ".jsx"],
        alias: {
            images: imagesDir
        }
    },
    entry: {
        app: path.join(srcDir, 'index.tsx')
    },
    output: {
        path: distDir,
        filename: `[name]${hash}.bundle.js`,
    },
    devtool: isProduction ? false : "cheap-module-source-map",
    module: {
        rules: [
            {
                test: /\.(sa|sc|c)ss$/,
                use: [{loader: MiniCssExtractPlugin.loader, options: {hmr: !isProduction}}, 'css-loader', 'sass-loader']
            },
            {
                test: /\.(ts|tsx|js|jsx)$/,
                exclude: /node_modules/,
                use: ['ts-loader' ]
            },
            {
                test: /\.html$/,
                exclude: /node_modules/,
                use: isProduction ? ['raw-loader', 'html-minify-loader'] : ['raw-loader']
            },
            {
                test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: `url-loader?name=resources/fonts/[name]${hash}.[ext]&limit=10000`
            },
            {
                test: /\.(ttf|eot)(\?[\s\S]+)?$/,
                use: `file-loader?name=resources/fonts/[name]${hash}.[ext]`
            },
            {
                test: /\.(png|jpg|gif|svg|ico)$/i,
                use: `url-loader?name=resources/images/[name]${hash}.[ext]&limit=10000`
            },

        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(srcDir, 'index.ejs'),
            title:"React TypeScript",
            hash: true,
            minify: {
                collapseWhitespace: true,
                removeComments: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true,
                useShortDoctype: true
            }
        }),
        new MiniCssExtractPlugin({
            filename: `[name]${hash}.css`,
            chunkFilename: `[id]${hash}.css`,
            ignoreOrder: true, // Enable to remove warnings about conflicting order
        }),
        // new CopyWebpackPlugin([
        // {
        //     from: path.join(resDir, 'static'),
        //     to: path.join(distDir, 'resources')
        // }]),
        // new webpack.ProvidePlugin({
        //     "React": "react",
        //     jQuery: 'jquery',
        //     $: 'jquery',
        //     jquery: 'jquery'
        // }),

        new webpack.HotModuleReplacementPlugin(),
    ],
    devServer: {
        open: true,
        contentBase: distDir,
        port:9001,
        stats:'errors-only',
        hot:true
    }
};

module.exports = config;
