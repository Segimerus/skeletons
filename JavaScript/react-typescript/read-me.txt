//Style Loading
"css-loader": css: The css-loader interprets @import and url() like import/require() and will resolve them,
"node-sass": sass requirement,
"sass-loader": converting sass to css,
"style-loader": required for css-loader,
"mini-css-extract-plugin": Responsible for bundling the css,

//Loading Resources
"file-loader": copy the imported files to dist with customizable path and config, the import returns the url
"url-loader": like file loader with the capability to convert files under the limit to base64 and inject them into source,
"raw-loader": lets you import file contents as string
"copy-webpack-plugin": Copies individual files or entire directories to the build directory,

//HTML
"html-minify-loader": minifies html,
"html-webpack-plugin": Needed for making the index.html file dynamically from template,

//Typescript
"ts-loader": loading typescript,
"typescript": speaks for itself

//WebPack Core
"webpack": required locally as well, for some of the plugins/loaders
"webpack-cli": "^3.3.6",
"webpack-dev-server": server, better use local

//Other
"rimraf" : implement unix's rm -rf command


***Notes***
- No hot reaload on modifying the template index.ejs
- url loader is an extended file loader
- url loader is not working for image src tags in static htmls, only for javascript imports and css url(), but you can use the copy plugin
- url loader does not like absolute paths only relative, you can use absolute paths with webpack aliases.
However in css you have to put the ~ mark in front to signal that it is an alias, of course webstorm does not recognizes it


