const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

const isProduction = process.argv.indexOf('-p') !== -1;

const cssProdConfig = ExtractTextPlugin.extract({fallback: "style-loader", use: ["css-loader"]});
const cssDevConfig = [ "style-loader","css-loader?sourceMap"];
const scssProdConfig = ExtractTextPlugin.extract({fallback: "style-loader", use: ["css-loader","sass-loader"]});
const scssDevConfig = [ "style-loader","css-loader?sourceMap","sass-loader?sourceMap"];
const htmlProdConfig = ['raw-loader', 'html-minify-loader'];
const htmlDevConfig = ['raw-loader'];
const devtoolProdConfig = false;
const devtoolDevConfig = "cheap-module-source-map";

const cssConfig = isProduction ? cssProdConfig : cssDevConfig;
const scssConfig = isProduction ? scssProdConfig : scssDevConfig;
const devtoolConfig = isProduction ? devtoolProdConfig : devtoolDevConfig;
const htmlConfig = isProduction ? htmlProdConfig : htmlDevConfig;

const srcDir = path.join(__dirname, 'src');
const distDir = path.join(__dirname, 'dist');

const config = {
    resolve: {
        alias:{
            Components: path.join(__dirname, 'src', 'components'),
            CommonStyles: path.join(__dirname, 'src', 'common', 'styles'),
            Constants: path.join(__dirname, 'src', 'common', 'constants'),
            State: path.join(__dirname, 'src', 'state'),
        }
    },
    entry: {
        app: path.join(srcDir, 'index.js')
    },
    output: {
        path: distDir,
        filename: '[name].bundle.js'
    },
    devtool: devtoolConfig,
    module: {
        rules: [
            {
                test: /\.css$/,
                use: cssConfig
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: scssConfig
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            },
            {
                test: /\.html$/,
                exclude: /node_modules/,
                use: htmlConfig
            },
            {
                test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: 'url-loader?name=resources/fonts/[name].[ext]&limit=10000',
            },
            {
                test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
                use: 'file-loader?name=resources/fonts/[name].[ext]',
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(srcDir, 'index.ejs'),
            title:"webpack skeleton",
            minify: {
                collapseWhitespace: true
            },
            hash: true
        }),
        new ExtractTextPlugin(
            {
                filename: "bundled.css",
                disable: !isProduction,
                allChunks:true
            }),
        new CopyWebpackPlugin([
            {
                from: path.join(srcDir, 'resources'),
                to: path.join(distDir, 'resources')
            }]),

        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            $: 'jquery',
            jquery: 'jquery'
        }),

        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
    ],
    devServer: {
        open: true,
        contentBase: distDir,
        port:9001,
        compress:true,
        stats:'errors-only',
        hot:true
    }
};

module.exports = config;