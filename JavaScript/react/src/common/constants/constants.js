export const featureList =
[
    {name: "ES6 WITH IMPORTABLE MODULES", status: "done"},
    {name: "LIVE CODE COMPILATION", status: "done"},
    {name: "HOT MODULE REPLACEMENT", status: "done"},
    {name: "SASS SUPPORT", status: "done"},
    {name: "IMPORTING STYLES AS MODULES", status: "done"},
    {name: "IMPORTING TEMPLATES AND OTHER RESOURCES AS MODULES", status: "done"},
    {name: "BUILD: MINIFY, UGLIFY", status: "done"},
    {name: "BUILD: ES5 TRANSPILATION", status: "done"},
    {name: "BUILD: BUNDLE THE FILES", status: "done"},
    {name: "SOURCE MAPS IN DEV MODE", status: "done"},
    {name: "UNIT TESTING SUPPORT", status: "missing"}
    // {name: "COMPONENTS", status: "done"},
    // {name: "ROUTING", status: "done"},
    // {name: "BACKEND INTEGRATION", status: "missing"},
    // {name: "FORM & VALIDATION", status: "missing"},
    // {name: "AUTHENTICATION", status: "missing"},

];