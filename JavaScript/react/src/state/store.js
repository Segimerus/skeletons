import {get as $get} from 'lodash'
import {createStore as $createStore} from 'redux'
import $actions from "./actions.js"

const initialState = {
    counter: {val: 0}
};

let store = $createStore(reducer);

/*defines how state changes from current to next*/
function reducer(state = initialState, action)
{
    switch (action.type)
    {
        case($actions.INC_COUNTER.type):
            const newVal = state.counter.val + action.payload;
            return { ...state, counter: { val: newVal}};
            break;
        default:
            return state;
    }

}

/*watch state for changes*/
export function watch(path,onChange)
{
    let currentState = getState(path);

    function handleChange()
    {
        let nextState = getState(path);

        if (nextState !== currentState)
        {
            currentState = nextState;
            onChange(currentState);
        }
    }

    return store.subscribe(handleChange);
}

/*initiate a statechange*/
export function dispatch(action, payload)
{
    store.dispatch(action.creator(payload));
}

/*get current state*/
export function getState(path)
{
    const currentState = store.getState();
    
    return $get(currentState, path, currentState);
}

