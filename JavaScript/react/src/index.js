import $reactDom from 'react-dom'
import React from 'react'
import "CommonStyles/main-page.scss"
import {featureList} from "Constants/constants.js"
import StatusBar from "Components/status-bar/status-bar.component.js"
import StoreTester from "Components/store-tester/store-tester.component.js"

renderUI();

function renderUI()
{
    $reactDom.render(
        <div>
            <StatusBar featureList={featureList}/>
            <StoreTester/>
        </div>, document.querySelector("#appContainer")
    );
}