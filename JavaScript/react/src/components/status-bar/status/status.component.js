import React from 'react'
import PropTypes from 'prop-types'

export default class Status extends React.Component {

    constructor(props)
    {
        super(props)
    }

    render()
    {
        return (
            <div className={`statusComponent ${this.props.status}`}>{this.props.name}</div>
        )
    }

};


Status.propTypes =
{
    name : PropTypes.string.isRequired,
    status : PropTypes.string.isRequired
}