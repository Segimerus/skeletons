import "./status-bar.component.scss"
import Status from "./status/status.component.js"
import React from 'react'
import PropTypes from 'prop-types'


export default class StatusBar extends React.Component
{
    constructor(props)
    {
        super(props)
    }

    render()
    {
        return(
            <div className="statusBarComponent">
                <div className="statusBarComponentContainer">
                    {renderFeatureList(this.props.featureList)}
                </div>
            </div>

        );

        function renderFeatureList(featureList)
        {
            return featureList.map((featureObject, index) =>
            {
                return (<Status key={`feature-${index}`} status={featureObject.status} name={featureObject.name}/>)
            })
        }
    }
}

StatusBar.propTypes = {
    featureList : PropTypes.array.isRequired
};




