import React from 'react'
import * as $store from 'State/store.js'
import $actions from 'State/actions.js'
import './store-tester.component.scss'

export default class StoreTester extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state =
        {
            incrementor: 1
        };

        this.inc = this.inc.bind(this);
        this.dec = this.dec.bind(this);
        this.incChange= this.incChange.bind(this);

        $store.watch("counter", () => this.forceUpdate());
        $store.watch("counter", (newState) => console.log(`State changed:`, newState, $store.getState()));
    }

    render()
    {
        const currentVal = $store.getState("counter").val;

        return (
            <div className="storeTester">
                <div className="storeCaption">Store Tester:</div>
                <input type="text" onChange={this.incChange} value={this.state.incrementor}/>
                <button onClick={this.inc}>+</button>
                <button onClick={this.dec}>-</button>
                <div className="storeVal">{currentVal}</div>
            </div>
        )
    }

    inc()
    {
        $store.dispatch($actions.INC_COUNTER, this.state.incrementor)
    }

    dec()
    {
        $store.dispatch($actions.INC_COUNTER, -1 * this.state.incrementor)
    }

    incChange(event)
    {
        this.setState({incrementor: parseInt(event.target.value)});
    }


}